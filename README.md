# Prometheus监控K8S资源文件

## 简介

本仓库提供了一系列用于在Kubernetes集群中部署Prometheus、Grafana和Node Exporter的YAML配置文件。通过这些资源文件，您可以快速搭建一个完整的监控系统，用于监控Kubernetes集群的各项指标。

## 文件列表

- `prometheus-deployment.yaml`: Prometheus的部署配置文件。
- `prometheus-service.yaml`: Prometheus的服务配置文件。
- `grafana-deployment.yaml`: Grafana的部署配置文件。
- `grafana-service.yaml`: Grafana的服务配置文件。
- `node-exporter-daemonset.yaml`: Node Exporter的DaemonSet配置文件。

## 使用方法

1. **克隆仓库**：
   ```bash
   git clone https://github.com/your-repo/prometheus-k8s-monitoring.git
   cd prometheus-k8s-monitoring
   ```

2. **部署Prometheus**：
   ```bash
   kubectl apply -f prometheus-deployment.yaml
   kubectl apply -f prometheus-service.yaml
   ```

3. **部署Grafana**：
   ```bash
   kubectl apply -f grafana-deployment.yaml
   kubectl apply -f grafana-service.yaml
   ```

4. **部署Node Exporter**：
   ```bash
   kubectl apply -f node-exporter-daemonset.yaml
   ```

5. **访问Grafana**：
   通过`kubectl port-forward`命令将Grafana服务暴露到本地：
   ```bash
   kubectl port-forward svc/grafana 3000:3000
   ```
   然后在浏览器中访问`http://localhost:3000`，使用默认的用户名`admin`和密码`admin`登录。

## 注意事项

- 请根据实际需求修改YAML文件中的配置，例如资源限制、存储卷等。
- 确保Kubernetes集群已经正确配置，并且具备足够的资源来运行这些组件。

## 贡献

欢迎提交Issue和Pull Request，帮助改进和完善本仓库的内容。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。